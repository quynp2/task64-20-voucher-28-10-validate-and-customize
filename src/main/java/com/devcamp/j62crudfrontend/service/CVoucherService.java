package com.devcamp.j62crudfrontend.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import com.devcamp.j62crudfrontend.model.CVoucher;
import com.devcamp.j62crudfrontend.repository.IVoucherRepository;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class CVoucherService {
    @Autowired
    IVoucherRepository pVoucherRepository;

    public ArrayList<CVoucher> getVoucherList() {
        ArrayList<CVoucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }

    public Optional<CVoucher> getVoucherById(long id) {
        return pVoucherRepository.findById(id);
    }

    public CVoucher createNewVoucher(CVoucher pVouchers) {
        pVouchers.setNgayTao(new Date());
        pVouchers.setNgayCapNhat(null);

        return pVoucherRepository.save(pVouchers);
    }

    public CVoucher updateVoucherById(CVoucher pVouchers) {
        return pVoucherRepository.save(pVouchers);
    }
}
