package com.devcamp.j62crudfrontend.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import java.util.*;

import com.devcamp.j62crudfrontend.model.CVoucher;
import com.devcamp.j62crudfrontend.repository.IVoucherRepository;
import com.devcamp.j62crudfrontend.service.CVoucherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CVoucherController {
	@Autowired
	IVoucherRepository pVoucherRepository;
	@Autowired
	CVoucherService pVoucherService;

	// Lấy danh sách voucher dùng service.
	@GetMapping("/vouchers") // Dùng phương thức GET
	public ResponseEntity<List<CVoucher>> getAllVouchersBySevice() {
		try {
			return new ResponseEntity<>(pVoucherService.getVoucherList(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	// Lấy danh sách voucher KHÔNG dùng service
	@GetMapping("/all-vouchers") // Dùng phương thức GET
	public ResponseEntity<List<CVoucher>> getAllVouchersNoService() {
		try {
			List<CVoucher> vouchers = new ArrayList<CVoucher>();
			pVoucherRepository.findAll().forEach(vouchers::add);
			return new ResponseEntity<>(vouchers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	// Lấy voucher theo {id} KHÔNG dùng service
	@GetMapping("/vouchers/{id}") // Dùng phương thức GET
	public ResponseEntity<CVoucher> getCVoucherByIdNoService(@PathVariable("id") long id) {
		try {
			Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
			if (voucherData.isPresent()) {
				return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	// Lấy voucher theo {id} dùng service
	@GetMapping("/voucherss/{id}") // Dùng phương thức GET
	public ResponseEntity<CVoucher> getCVoucherById(@PathVariable("id") long id) {
		try {
			Optional<CVoucher> voucherData = pVoucherService.getVoucherById(id);
			if (voucherData.isPresent()) {
				return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	// Tạo MỚI voucher KHÔNG dùng service sử dụng phương thức POST
	@PostMapping("/vouchers") // Dùng phương thức POST
	public ResponseEntity<Object> createCVoucherNoService(@Valid @RequestBody CVoucher pVouchers) {
		try {

			Optional<CVoucher> voucherData = pVoucherRepository.findById(pVouchers.getId());
			if (voucherData.isPresent()) {
				return ResponseEntity.unprocessableEntity().body(" Voucher already exsit ");
			}
			pVouchers.setNgayTao(new Date());
			pVouchers.setNgayCapNhat(null);
			CVoucher _vouchers = pVoucherRepository.save(pVouchers);
			return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " +
					e.getCause().getCause().getMessage());
			// Hiện thông báo lỗi tra back-end
			// return new ResponseEntity<>(e.getCause().getCause().getMessage(),
			// HttpStatus.INTERNAL_SERVER_ERROR);
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " +
							e.getCause().getCause().getMessage());
		}
	}

	// Tạo MỚI voucher DUNG service sử dụng phương thức POST
	@PostMapping("/voucherss") // Dùng phương thức POST
	public ResponseEntity<Object> createCVoucher(@RequestBody CVoucher pVouchers) {
		try {

			Optional<CVoucher> voucherData = pVoucherRepository.findById(pVouchers.getId());
			if (voucherData.isPresent()) {
				return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
			}

			CVoucher _vouchers = pVoucherService.createNewVoucher(pVouchers);
			return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			// Hiện thông báo lỗi tra back-end
			// return new ResponseEntity<>(e.getCause().getCause().getMessage(),
			// HttpStatus.INTERNAL_SERVER_ERROR);
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
	}

	// Sửa/update voucher theo {id} KHÔNG dùng service, sử dụng phương thức PUT
	@PutMapping("/vouchers/{id}") // Dùng phương thức PUT
	public ResponseEntity<Object> updateCVoucherById(@PathVariable("id") long id, @RequestBody CVoucher pVouchers) {
		Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
			CVoucher voucher = voucherData.get();
			if (pVouchers.getMaVoucher() != null && !pVouchers.getMaVoucher().isEmpty()) {
				voucher.setMaVoucher(pVouchers.getMaVoucher());

			}
			if (pVouchers.getPhanTramGiamGia() != null && !pVouchers.getPhanTramGiamGia().isEmpty()) {
				voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());

			}
			if (pVouchers.getGhiChu() != null && !pVouchers.getGhiChu().isEmpty()) {
				voucher.setGhiChu(pVouchers.getGhiChu());

			}
			voucher.setNgayCapNhat(new Date());
			try {
				return new ResponseEntity<>(pVoucherRepository.save(voucher), HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Voucher:" + e.getCause().getCause().getMessage());
			}
		} else {
			return ResponseEntity.badRequest().body("Failed to get specified Voucher: " + id + "  for update.");
		}
	}

	// Sửa/update voucher theo {id} dùng service, sử dụng phương thức PUT
	// @PutMapping("/vouchers/{id}") // Dùng phương thức PUT
	// public ResponseEntity<Object> updateCVoucherById(@PathVariable("id") long id,
	// @RequestBody CVoucher pVouchers) {
	// Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
	// if (voucherData.isPresent()) {
	// CVoucher voucher = voucherData.get();
	// voucher.setMaVoucher(pVouchers.getMaVoucher());
	// voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
	// voucher.setGhiChu(pVouchers.getGhiChu());
	// voucher.setNgayCapNhat(new Date());
	// try {
	// return new ResponseEntity<>(pVoucherService.updateVoucherById(voucher),
	// HttpStatus.OK);
	// } catch (Exception e) {
	// return ResponseEntity.unprocessableEntity()
	// .body("Failed to Update specified Voucher:" +
	// e.getCause().getCause().getMessage());
	// }
	// } else {
	// return ResponseEntity.badRequest().body("Failed to get specified Voucher: " +
	// id + " for update.");
	// }
	// }

	// Xoá/delete voucher theo {id} KHÔNG dùng service, sử dụng phương thức DELETE
	@DeleteMapping("/vouchers/{id}") // Dùng phương thức DELETE
	public ResponseEntity<CVoucher> deleteCVoucherById(@PathVariable("id") long id) {
		try {
			pVoucherRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Xoá/delete tất cả voucher KHÔNG dùng service, sử dụng phương thức DELETE
	@DeleteMapping("/vouchers") // Dùng phương thức DELETE
	public ResponseEntity<CVoucher> deleteAllCVoucher() {
		try {
			pVoucherRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
